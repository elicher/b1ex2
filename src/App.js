import logo from './logo.svg';
import './App.css';

const products = [
  {
    product: "flash t-shirt",
    price: 27.5,
    category: "t-shirts",
    bestSeller: false,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg",
    onSale: true,
  },
  {
    product: "batman t-shirt",
    price: 22.5,
    category: "t-shirts",
    bestSeller: true,
    image:
      "https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png",
    onSale: false,
  },
  {
    product: "superman hat",
    price: 13.9,
    category: "hats",
    bestSeller: true,
    image:
      "https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg",
    onSale: false,
  },
];


function App() {

let renderBest = (array) => (
  array.map((ele,index)=>{
      if (ele.onSale===true){
        var check="SALE"
      }
    if (ele.bestSeller===true){
      return <article key={index} className="card">
      <img key={index} src={ele.image} className="image"/>
      <h2 key={index}>{ele.product}</h2>
      <p key={index}>{ele.category}</p>
      <div key={index} className="buyNow">
      <p key={index}>{ele.price}</p>
      <button key={index}>BUY</button>
      <span class="sale">{check}</span>
      </div>
      </article>
    }
  }))

let renderProducts = (array) => (
    array.map((ele,index)=>{
      if (ele.onSale===true){
        var check="SALE"
      }
      return <article key={index} className="card">
      <img key={index} src={ele.image} className="image"/>
      <h2 key={index}>{ele.product}</h2>
      <p key={index}>{ele.category}</p>
      <div key={index} className="buyNow">
      <p key={index}>{ele.price}</p>
      <button key={index}>BUY</button>
      <span class="sale">{check}</span>
      </div>
      </article>
    })
    )

  return (
    <div className="App">

      <header className="App-header">
    <h1 className="h1header">My shop</h1>
    <ul id="header-ul">
    <li className="list-item">Item 1</li>
    <li className="list-item">Item 2</li>
    <li className="list-item">Item3</li>
    </ul>
   </header>

<main>   
<h1>PRODUCTS</h1>
   <div className="cards">

{
renderProducts(products)
}
   </div>
<h1>BEST SELLERS</h1>
<div className="cards">
{
renderBest(products)
}
</div>
</main>
   <footer className="footerContainer">
   <div className="footerDesc">
   <h1>My shop</h1>
   <p>All rights reserved</p>
   </div>
   </footer>


    </div>
  );
}

export default App;
